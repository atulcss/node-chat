const join = () => {
    let username = $('#name').val();
    let user_id = Date.now() + Math.random();
    user_id = user_id.toString().replace(".","_");
    let users = {
        user_name : username,
        user_id : user_id
    }
    socket.emit('joined', users);
    $('#join').hide();
                $('#after-join').show();
                sessionStorage.setItem("user", JSON.stringify(users));
                $('#me').html(`
                        <div class="me">
                            <img src="images/" />
                            ${users.user_name}
                         </div>
                         `);
}

const sendMyMessage = (chatWidowId, fromUser, message) => {
    let joinedUser = JSON.parse(sessionStorage.getItem('user'))
    let meClass = joinedUser.user_id == fromUser.user_id ? 'me' : '';

    $('#after-join').find(`#${chatWidowId} .body`).append(`
        <div class="chat-text ${meClass}">
            <div class="userPhoto">
                <img src="images/" />
            </div>
            <div>
                <span class="message">${message}<span>
            </div>
        </div>
    `);
}

const sendMessage = (room) => {
    let joinedUser = JSON.parse(sessionStorage.getItem('user'))
    let message = $('#'+room).find('.messageText').val();
    $('#'+room).find('.messageText').val('');
    socket.emit('message', {room: room, message:message, from: joinedUser});
    sendMyMessage(room, joinedUser, message)
}
const openChatWindow = (room,name) => {
    if($(`#${room}`).length === 0 ) {
        $('#after-join').append(`
        <div class="chat-window" id="${room}">
            <div class="body">${name}</div>
            <div class="footer">
                <input type="text" class="messageText"/><button onclick="sendMessage('${room}')">GO</button>
            </div>
        </div>
        `)
    }
}
const createRoom = (id,name) => {
    let joinedUser = JSON.parse(sessionStorage.getItem('user'));
    let room = Date.now() + Math.random();
    room = room.toString().replace(".","_");
    socket.emit('create', {room: room, userId:joinedUser.userId, withUserId:id});
    openChatWindow(room,name);
}
socket.on('updateUserList', function(userList) {
    let joinedUser = JSON.parse(sessionStorage.getItem('user'));
    $('#user-list').html('<ul></ul>');
    userList.forEach(item => {
        if(joinedUser.user_id != item.user_id){
            $('#user-list ul').append(`<li data-id="${item.user_id}" onclick="createRoom('${item.user_id}','${item.user_name}')">${item.user_name}</li>`)
        }
    });

});

socket.on('invite', function(data) {
    socket.emit("joinRoom",data)
});
socket.on('message', function(msg) {
    //If chat window not opened with this roomId, open it
    if(!$('#after-join').find(`#${msg.room}`).length) {
        openChatWindow(msg.room,msg.from.user_name)
    }
    sendMyMessage(msg.room, msg.from, msg.message)
});